<?php
//Assignment_2// 
// Database name
$Product = "Product.db";

// Database Connection
$db = new SQLite3($Product);

// Create Table "Product" into Database if not exists 
$query = "CREATE TABLE IF NOT EXISTS Product (Id INTEGER PRIMARY KEY AUTOINCREMENT,Product_name STRING, Cost INT)";
$db->exec($query);

?>